var assert = require('assert');
describe('RandomFail', function() {
    it('randomly fail the unit test', function() {
        var randomNumber = Math.floor(Math.random() * 10);
        assert.notEqual(randomNumber, 3);
    });
});

